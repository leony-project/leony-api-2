<?php

namespace App\Controller;

use App\Controller\Lending\PatchLending;
use App\Controller\Lending\PostLending;
use App\Entity\Lending;
use App\Entity\RfidTag;
use App\Service\EntityService;
use App\Service\RfidTagService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class RfidController extends AbstractController
{
    private $JwtManager;
    private $rfidTagService;
    private $entityService;
    private $userService;

    public function __construct(
        JWTTokenManagerInterface $JwtManager,
        RfidTagService $rfidTagService,
        EntityService $entityService,
        UserService $userService,
        PatchLending $patchLending,
        PostLending $postLending
    )
    {
        $this->JwtManager = $JwtManager;
        $this->rfidTagService = $rfidTagService;
        $this->entityService = $entityService;
        $this->userService = $userService;
        $this->patchLending = $patchLending;
        $this->postLending = $postLending;
    }

    /**
     * @Route("/rfid-login", name="rfid_login", methods={"POST"})
     */
    public function login(Request $request)
    {
        $id = $request->query->get('uid');
        if (!$id) throw $this->createNotFoundException('Uid undefined');

        $tag = $this->rfidTagService->getOneById($id);

        if (!$tag) {
            $this->rfidTagService->register($id);
            return new Response('Tag registered successfully');
        }

        $user = $tag->getUser();
        if (!$user) throw $this->createNotFoundException('User undefined');

        $jwt = $this->JwtManager->create($user);
        return new Response($jwt);
    }

    /**
     * @Route("/api/rfid/lending", name="rfid_lending", methods={"POST"})
     */
    public function lending(Request $request)
    {
        $id = $request->query->get('uid');
        if (!$id) throw $this->createNotFoundException('Uid undefined');

        $tag = $this->rfidTagService->getOneById($id);
        $entityManager = $this->getDoctrine()->getManager();

        if (!$tag) {
            $this->rfidTagService->register($id);
            return new Response('Tag registered successfully');
        }

        $entity = $tag->getEntity();
        if (!$entity) throw $this->createNotFoundException('Entity undefined');
        $lending = $this->entityService->isLended($entity->getId());

        if($lending) {

            $lending->setUserReturn($this->getUser());
            $lending->setReturnDatetime(new \DateTime());
            $lending->setReturnQuantity($lending->getLendingQuantity());
            $lending->setReturned(true);
            $lending->setOldReturned(true);
            $lending->setUserReturn($this->getUser());
            $entityManager->flush();
            $this->patchLending->__invoke($lending);
            return new Response('Entity ' . $entity->getReference() . ' returned successfully');

        } else {

            $lending = new Lending();
            $lending->setEntity($entity);
            $lending->setUserLending($this->getUser());
            $lending->setLendingDatetime(new \DateTime());
            $lending->setLendingQuantity($lending->getLendingQuantity());
            $lending->setAccepted(true);
            $lending->setReturned(false);
            $lending->setOldReturned(false);
            $this->postLending->__invoke($lending);
            $entityManager->persist($lending);
            $entityManager->flush();
            return new Response('Entity ' . $entity->getReference() . ' lended successfully');

        }
    }
}
