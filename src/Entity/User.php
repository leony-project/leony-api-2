<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\UserRepository;
use App\Controller\GetMe;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Controller\User\GetUsers;


/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *             "security"="is_granted('ROLE_USER')",
 *             "controller"=GetUsers::class,
 *             "normalization_context"={"groups"={"userList:read"}},
 *         },
 *         "post",
 *         "get_me"={
 *              "method"="GET",
 *              "path"="/users/get/me",
 *              "security"="is_granted('ROLE_USER')",
 *              "controller"=Getme::class,
 *              "normalization_context"={"groups"={"user:read"}}
 *          }
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_USER') and object == user or is_granted('ROLE_ADMIN')"},
 *         "put"={"security"="is_granted('ROLE_USER') and object == user or is_granted('ROLE_ADMIN')"},
 *         "patch"={"security"="is_granted('ROLE_USER') and object == user or is_granted('ROLE_ADMIN')"},
 *         "delete"={"security"="is_granted('ROLE_USER') and object == user or is_granted('ROLE_ADMIN')"}
 *     },
 *     denormalizationContext={"groups"={"user:post"}},
 *     normalizationContext={
 *         "groups"={"user:read"},
 *         "skip_null_values" = false,
 *     },
 *     attributes={
 *         "security"="is_granted('ROLE_ADMIN')",
 *          "order"={"id": "DESC"},
 *     },
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * 
 * @UniqueEntity(
 *     fields={"email"},
 *     message="'{{ value }}' is already used"
 * )
 * @ApiFilter(SearchFilter::class, properties={"lastName": "partial", "firstName": "partial", "email": "partial"})
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"entity-lending:read","lending:read","user:read","user-lending:read","user:post","userList:read"})
     **/
    private $id;

    /**
     * @ORM\Column(type="string", length=191, unique=true)
     * @Groups({"entity-lending:read","state-lending:read","user:post","lending:read","user:read","user-lending:read","userList:read"})
     * @Assert\NotBlank(
     *     message = "Email must be filled"
     * )
     *
     * @Assert\Email(
     *     message = "'{{ value }}' is not a valid email"
     * )
     */
    private $email;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @Groups({"user:post"})
     * @SerializedName("password")
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"entity-lending:read","state-lending:read","user:post","lending:read","user:read","user-lending:read","userList:read"})
     * @Assert\NotBlank(
     *     message = "First name must be filled"
     * )
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"entity-lending:read","state-lending:read","user:post","lending:read","user:read","user-lending:read","userList:read"})
     * @Assert\NotBlank(
     *     message = "Last name must be filled"
     * )
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user:read","user:post","userList:read"})
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user:read","user:post","userList:read"})
     */
    private $git;

    /**
     * @ORM\OneToMany(targetEntity=Lending::class, mappedBy="userLending", orphanRemoval=true)
     * @ApiSubresource
     */
    private $lendings;

    /**
     * @ORM\Column(type="array")
     * @Groups({"user_admin:read"})
     */
    private $roles = [];

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"user:read","user:post"})
     * 
     * @Assert\NotNull(
     *     message = "IsAdmin boolean must be filled"
     * )
     */
    private $isAdmin;

    public function __construct()
    {
        $this->lendings = new ArrayCollection();
        $this->addRole('ROLE_USER');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getGit(): ?string
    {
        return $this->git;
    }

    public function setGit(?string $git): self
    {
        $this->git = $git;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        return array_unique($roles);
    }

    /**
     * Add role
     */
    public function addRole(string $role): self
    {
        $roles = $this->getRoles();

        array_push($roles, $role);

        $this->setRoles($roles);

        return $this;
    }

    /**
     * Has role
     */
    public function hasRole(string $checkRole): bool
    {
        foreach ($this->roles as $role) {
            if ($role === $checkRole) {
                return true;
            }
        }

        return false;
    }

    /**
     * Remove role
     */
    public function removeRole(string $removeRole)
    {
        $newRoles = [];

        foreach ($this->roles as $role) {
            if ($role !== $removeRole) {
                $newRoles[] = $role;
            }
        }

        $this->setRoles($newRoles);
    }


    /**
     * @return Collection|Lending[]
     */
    public function getLendings(): Collection
    {
        return $this->lendings;
    }

    public function addLending(Lending $lending): self
    {
        if (!$this->lendings->contains($lending)) {
            $this->lendings[] = $lending;
            $lending->setUserLending($this);
        }

        return $this;
    }

    public function removeLending(Lending $lending): self
    {
        if ($this->lendings->contains($lending)) {
            $this->lendings->removeElement($lending);
            // set the owning side to null (unless already changed)
            if ($lending->getUserLending() === $this) {
                $lending->setUserLending(null);
            }
        }

        return $this;
    }


    public function getIsAdmin(): ?bool
    {
        return $this->isAdmin;
    }

    public function setIsAdmin(?bool $isAdmin): self
    {
        $this->isAdmin = $isAdmin;

        if ($isAdmin === true) {
            $this->addRole('ROLE_ADMIN');
        } else {
            $this->removeRole('ROLE_ADMIN');
        }

        return $this;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
}
