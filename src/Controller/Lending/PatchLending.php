<?php

namespace App\Controller\Lending;

use App\Entity\Lending;
use App\Service\LendingService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class PatchLending
{
    /** @var LendingService */
    private $lendingService;


    public function __construct( LendingService $lendingService)
    {
        $this->lendingService = $lendingService;
    }

    public function __invoke(Lending $data)
    {
        // check entity availability.
        if ($data->getReturned() === false) {
            if ($data->getEntity() !== null) {
                $data->getEntity()->setAvailable(false);
            }
        } else {
            if ($data->getEntity() !== null) {
                $data->getEntity()->setAvailable(true);
            }
        }

        // Check stock
        if ($data->getReturned() === true && $data->getOldReturned() === false) {
            if ($data->getConsumable() !== null && $data->getReturnQuantity() !== null) {
                $data->getConsumable()->setQuantity(
                    ($data->getConsumable()->getQuantity() === null ? 0 : $data->getConsumable()->getQuantity()) +
                    intval($data->getReturnQuantity())
                );
            }
        }

        if ($data->getReturned() === false && $data->getOldReturned() === true) {
            if ($data->getConsumable() !== null && $data->getLendingQuantity() !== null) {
                $newQuantity = ($data->getConsumable()->getQuantity() === null ? 0 : $data->getConsumable()->getQuantity()) - $data->getLendingQuantity();

                if ($newQuantity < 0) {
                    throw new BadRequestException('Insufficient stocks');
                }

                $data->getConsumable()->setQuantity($newQuantity);
            }
        }

        $data->setOldReturned($data->getReturned());

        return $data;
    }
}
