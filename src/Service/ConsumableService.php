<?php

namespace App\Service;

use App\Repository\ConsumableRepository;

class ConsumableService
{
    private $consumable_repo;

    public function __construct(
        ConsumableRepository $consumable_repo
    )
    {
        $this->consumable_repo = $consumable_repo;
    }

    public function getConsumable($id)
    {
        return $this->consumable_repo->find($id);
    }
}
