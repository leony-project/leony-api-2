<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserService
{
    private $tokenStorage;

    /** @var UserRepository */
    private $userRepository;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        UserRepository $userRepository
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->userRepository =$userRepository;
    }

    public function getLoggedUser(): User
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    public function getUsers()
    {
        return $this->userRepository->getUsers();
    }
}
