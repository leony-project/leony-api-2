<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\UserService;

class GetMe
{
    /** @var UserService */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService =  $userService;
    }
    
    public function __invoke($data): User
    {
        return $this->userService->getLoggedUser();
    }
}
