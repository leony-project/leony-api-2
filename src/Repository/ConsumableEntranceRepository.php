<?php

namespace App\Repository;

use App\Entity\ConsumableEntrance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ConsumableEntrance|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConsumableEntrance|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConsumableEntrance[]    findAll()
 * @method ConsumableEntrance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConsumableEntranceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConsumableEntrance::class);
    }

    // /**
    //  * @return ConsumableEntrance[] Returns an array of ConsumableEntrance objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConsumableEntrance
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
