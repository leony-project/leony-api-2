<?php


namespace App\DataFixtures;

use App\Entity\Consumable;
use App\Entity\Equipment;
use App\Entity\Lending;
use App\Entity\MediaObject;
use App\Entity\StockPlace;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture implements FixtureGroupInterface
{

    const USER_1 = "USER_1";
    const USER_2 = "USER_2";
    const PASSWORD = "password";
    const LINKGIT = "https://github.com/symfony/symfony";

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $users = [
            ["firstname" => "Admin", "lastname" => "Admin", "email" => "admin@leony.fr", "phone" => "+33600000000", "git" => self::LINKGIT, "password" => self::PASSWORD, "admin" => true, "addreference" => self::USER_1],
            ["firstname" => "Student", "lastname" => "Student", "email" => "student@leony.fr", "phone" => "+33600000000", "git" => self::LINKGIT, "password" => self::PASSWORD, "admin" => false, "addreference" => self::USER_2],
        ];

        $users_entities = [];
        foreach ($users as $newUser) {
            $user = new User();
            $user->setFirstName($newUser["firstname"])
                ->setLastName($newUser["lastname"])
                ->setEmail($newUser["email"])
                ->setIsAdmin($newUser["admin"])
                ->setPhone($newUser["phone"])
                ->setGit($newUser["git"])
                ->setPassword($this->passwordEncoder->encodePassword(
                    $user,
                    $newUser['password']
                ));
            $users_entities[] = $user;
            $manager->persist($user);

            $this->addReference($newUser['addreference'], $user);
        }

        $stockplaces = [
            ["name" => "Armoire"],
            ["name" => "Bureau"]
        ];

        $stockplaces_entities = [];
        foreach($stockplaces as $newstockplace) {
            $stockplace = new StockPlace();
            $stockplace->setName($newstockplace['name']);
            $stockplaces_entities[] = $stockplace;
            $manager->persist($stockplace);
        }

        $media_objects = [
            ["file_path" => "raspberry.png"],
            ["file_path" => "marteau.jpg"],
            ["file_path" => "encre.jpg"]
        ];

        $medias_entities = [];
        foreach($media_objects as $newmedia) {
            $media = new MediaObject();
            $media->setFilePath($newmedia['file_path']);

            $medias_entities[] = $media;
            $manager->persist($media);
        }

        $equipments = [
            ["name" => "Raspberry", "stock_place" => $stockplaces_entities[0], "image" => $medias_entities[0], "description" => "Carte electronique"],
            ["name" => "Marteau", "stock_place" => $stockplaces_entities[1], "image" => $medias_entities[1], "description" => "Outil pour taper"]
        ];

        $equipments_entities = [];
        foreach($equipments as $newequipment) {
            $equipment = new Equipment();
            $equipment->setName($newequipment['name']);
            $equipment->setDescription($newequipment['name']);
            $equipment->setEnabled(1);
            $equipment->setImage($newequipment['image']);
            $equipment->setStockPlace($newequipment['stock_place']);

            $equipments_entities[] = $equipment;
            $manager->persist($equipment);
        }

        $consumables = [
            ["name" => "Pot d'encre", "stock_place" => $stockplaces_entities[0], "image" => $medias_entities[2], "description" => "Pot d'encre noir"],
        ];

        $consumables_entities = [];
        foreach($consumables as $newconsumable) {
            $consumable = new Consumable();
            $consumable->setName($newconsumable['name']);
            $consumable->setDescription($newconsumable['name']);
            $consumable->setEnabled(1);
            $consumable->setQuantity(10);
            $consumable->setUnity("pot");
            $consumable->setImage($newconsumable['image']);
            $consumable->setStockPlace($newconsumable['stock_place']);

            $consumables_entities[] = $consumable;
            $manager->persist($consumable);
        }

        $lendings = [
            ["user_lending" => $users_entities[1], "description" => "Pot d'encre noir emprunté", "quantity" => 5],
        ];

        $lendings_entities = [];
        foreach($lendings as $newlending) {
            $lending = new Lending();
            $lending->setUserLending($newlending['user_lending']);
            $lending->setCommentLending($newlending['description']);
            $lending->setConsumable($consumables_entities[0]);
            $lending->setReturned(0);
            $lending->setOldReturned(0);
            $lending->setAccepted(1);
            $lending->setLendingDatetime(new \DateTime('NOW'));
            $return_date = new \DateTime('NOW');
            $return_date->modify('+5 day');
            $lending->setReturnDatetime($return_date);
            $lending->setLendingQuantity($newlending['quantity']);
            $lending->setState("");

            $lendings_entities[] = $lending;
            $manager->persist($lending);
        }

        $manager->flush();
    }
    
    public static function getGroups(): array
    {
        return ['dev','prod'];
    }
}
