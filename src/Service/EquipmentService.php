<?php

namespace App\Service;

use App\Repository\EquipmentRepository;

class EquipmentService
{
    private $equipment_repo;

    public function __construct(
        EquipmentRepository $equipment_repo
    )
    {
        $this->equipment_repo = $equipment_repo;
    }

    public function getEquipment($id)
    {
        return $this->equipment_repo->find($id);
    }
}
