<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ConsumableRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *    subresourceOperations={
 *    "api_tags_consumables_get_subresource"={
 *      "method"="GET",
 *      "normalization_context"={"groups"={"tags-consumable:read"}},
 *      "pagination_enabled"=true
 *    },
 *    "api_stockplace_consummable_get_subresource"={
 *      "method"="GET",
 *      "security"="is_granted('ROLE_ADMIN')",
 *      "normalization_context"={"groups"={"stockplace-consummable:read"}},
 *      "pagination_enabled"=true
 *    },
 *    "api_equipment_consumables_get_subresource"={
 *      "method"="GET",
 *      "normalization_context"={"groups"={"consumable:read"}},
 *      "pagination_enabled"=true
 *    }
 *   },
 *   attributes={"order"={"id": "DESC"}},
 *   normalizationContext={
 *    "groups"={"consumable:read"},
 *    "skip_null_values" = false
 *  },
 * 
 * 
 *  itemOperations={
 *      "get",
 *      "put"={"security"="is_granted('ROLE_ADMIN')"},
 *      "patch"={"security"="is_granted('ROLE_ADMIN')"},
 *      "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *  },
 * 
 * 
 *  collectionOperations={
 *      "get",
 *      "post"={"security"="is_granted('ROLE_ADMIN')"}
 *  }
 * )
 * @ORM\Entity(repositoryClass=ConsumableRepository::class)
 * @ApiFilter(SearchFilter::class, properties={"name": "partial"})
 */
class Consumable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"consumable:read","stockplace:read","tags-consumable:read","entity-lending:read","lending:read","user-lending:read","consumableEntrance:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"consumable:read","stockplace:read","tags-consumable:read","entity-lending:read","lending:read","user-lending:read","consumableEntrance:read","stockplace-consummable:read"})
     * 
     * @Assert\NotBlank(
     *     message = "Name must be filled"
     * )
     */
    private $name;

    /**
     * @var MediaObject|null
     * 
     * @ORM\ManyToOne(targetEntity=MediaObject::class, cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"consumable:read","lending:read","stockplace:read","stockplace-consummable:read","tags-consumable:read"})
     */
    public $image;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"consumable:read","tags-consumable:read","entity-lending:read","lending:read","user-lending:read"})
     */
    private $quantity;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"consumable:read","tags-consumable:read","entity-lending:read","lending:read","user-lending:read"})
     * 
     * @Assert\NotNull(
     *     message = "Enabled boolean must be filled"
     * )
     */
    private $enabled;


    /**
     * @ORM\ManyToMany(targetEntity=Tag::class, mappedBy="consumable")
     * @Groups({"consumable:read","entity-lending:read","lending:read","user-lending:read"})
     */
    private $tags;

    /**
     * @ORM\ManyToMany(targetEntity=Equipment::class, mappedBy="consumables")
     * @Groups({"consumable:read","entity-lending:read","lending:read","user-lending:read"})
     */
    private $equipments;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"consumable:read","entity-lending:read","lending:read","user-lending:read"})
     * 
     * @Assert\NotBlank(
     *     message = "Unity must be filled"
     * )
     */
    private $unity;

    /**
     * @ORM\OneToMany(targetEntity=Lending::class, mappedBy="consumable")
     */
    private $lendings;

    /**
     * @ORM\OneToMany(targetEntity=ConsumableEntrance::class, mappedBy="consumable", orphanRemoval=true)
     * @ApiSubresource
     */
    private $consumableEntrances;

    /**
     * @ORM\ManyToOne(targetEntity=StockPlace::class, inversedBy="consumables")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"consumable:read"})
     */
    private $stockPlace;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"entity:read","equipment:read","consumable:read"})
     */
    private $description;

    public function __construct()
    {
        $this->equipments = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->lendings = new ArrayCollection();
        $this->consumableEntrances = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?MediaObject
    {
        return $this->image;
    }

    public function setImage(?MediaObject $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(float $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->addConsumable($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            $tag->removeConsumable($this);
        }

        return $this;
    }

    /**
     * @return Collection|Equipment[]
     */
    public function getEquipments(): Collection
    {
        return $this->equipments;
    }

    public function addEquipment(Equipment $equipment): self
    {
        if (!$this->equipments->contains($equipment)) {
            $this->equipments[] = $equipment;
            $equipment->addConsumable($this);
        }

        return $this;
    }

    public function removeEquipment(Equipment $equipment): self
    {
        if ($this->equipments->contains($equipment)) {
            $this->equipments->removeElement($equipment);
            $equipment->removeConsumable($this);
        }

        return $this;
    }

    public function getUnity(): ?string
    {
        return $this->unity;
    }

    public function setUnity(string $unity): self
    {
        $this->unity = $unity;

        return $this;
    }

    /**
     * @return Collection|Lending[]
     */
    public function getLendings(): Collection
    {
        return $this->lendings;
    }

    public function addLending(Lending $lending): self
    {
        if (!$this->lendings->contains($lending)) {
            $this->lendings[] = $lending;
            $lending->setConsumable($this);
        }

        return $this;
    }

    public function removeLending(Lending $lending): self
    {
        if ($this->lendings->contains($lending)) {
            $this->lendings->removeElement($lending);
            // set the owning side to null (unless already changed)
            if ($lending->getConsumable() === $this) {
                $lending->setConsumable(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ConsumableEntrance[]
     */
    public function getConsumableEntrances(): Collection
    {
        return $this->consumableEntrances;
    }

    public function addConsumableEntrance(ConsumableEntrance $consumableEntrance): self
    {
        if (!$this->consumableEntrances->contains($consumableEntrance)) {
            $this->consumableEntrances[] = $consumableEntrance;
            $consumableEntrance->setConsumable($this);
        }

        return $this;
    }

    public function removeConsumableEntrance(ConsumableEntrance $consumableEntrance): self
    {
        if ($this->consumableEntrances->contains($consumableEntrance)) {
            $this->consumableEntrances->removeElement($consumableEntrance);
            // set the owning side to null (unless already changed)
            if ($consumableEntrance->getConsumable() === $this) {
                $consumableEntrance->setConsumable(null);
            }
        }

        return $this;
    }

    public function getStockPlace(): ?StockPlace
    {
        return $this->stockPlace;
    }

    public function setStockPlace(?StockPlace $stockPlace): self
    {
        $this->stockPlace = $stockPlace;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

}
