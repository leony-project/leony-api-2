<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230215152642 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE consumable (id INT AUTO_INCREMENT NOT NULL, image_id INT DEFAULT NULL, stock_place_id INT NOT NULL, name VARCHAR(255) NOT NULL, quantity DOUBLE PRECISION DEFAULT NULL, unity VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, enabled TINYINT(1) NOT NULL, INDEX IDX_4475F0953DA5256D (image_id), INDEX IDX_4475F0954260F65B (stock_place_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE consumable_entrance (id INT AUTO_INCREMENT NOT NULL, consumable_id INT NOT NULL, entrance TINYINT(1) NOT NULL, quantity DOUBLE PRECISION NOT NULL, date DATETIME NOT NULL, enabled TINYINT(1) NOT NULL, INDEX IDX_FCEA8024A94ADB61 (consumable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entity (id INT AUTO_INCREMENT NOT NULL, equipment_id INT NOT NULL, enabled TINYINT(1) NOT NULL, reference VARCHAR(255) NOT NULL, available TINYINT(1) NOT NULL, state VARCHAR(255) NOT NULL, INDEX IDX_E284468517FE9FE (equipment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equipment (id INT AUTO_INCREMENT NOT NULL, image_id INT DEFAULT NULL, stock_place_id INT NOT NULL, name VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_D338D5833DA5256D (image_id), INDEX IDX_D338D5834260F65B (stock_place_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equipment_consumable (equipment_id INT NOT NULL, consumable_id INT NOT NULL, INDEX IDX_6D7E044B517FE9FE (equipment_id), INDEX IDX_6D7E044BA94ADB61 (consumable_id), PRIMARY KEY(equipment_id, consumable_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equipment_tag (equipment_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_32097FE2517FE9FE (equipment_id), INDEX IDX_32097FE2BAD26311 (tag_id), PRIMARY KEY(equipment_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lending (id INT AUTO_INCREMENT NOT NULL, entity_id INT DEFAULT NULL, user_lending_id INT NOT NULL, user_return_id INT DEFAULT NULL, consumable_id INT DEFAULT NULL, lending_datetime DATETIME DEFAULT NULL, return_datetime DATETIME DEFAULT NULL, returned TINYINT(1) NOT NULL, old_returned TINYINT(1) DEFAULT \'0\' NOT NULL, accepted TINYINT(1) NOT NULL, comment_lending LONGTEXT DEFAULT NULL, comment_return LONGTEXT DEFAULT NULL, lending_quantity INT DEFAULT NULL, return_quantity INT DEFAULT NULL, state LONGTEXT NOT NULL, enabled TINYINT(1) NOT NULL, INDEX IDX_74AB8C0381257D5D (entity_id), INDEX IDX_74AB8C03155210D6 (user_lending_id), INDEX IDX_74AB8C0322949B31 (user_return_id), INDEX IDX_74AB8C03A94ADB61 (consumable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE logging (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, data LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_CA9562FAA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media_object (id INT AUTO_INCREMENT NOT NULL, file_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parameter (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, value LONGTEXT NOT NULL, category VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rfid_tag (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, entity_id INT DEFAULT NULL, uid VARCHAR(50) NOT NULL, enabled TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_4FDECE21539B0606 (uid), INDEX IDX_4FDECE21A76ED395 (user_id), UNIQUE INDEX UNIQ_4FDECE2181257D5D (entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stock_place (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, color VARCHAR(30) NOT NULL, enabled TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag_consumable (tag_id INT NOT NULL, consumable_id INT NOT NULL, INDEX IDX_E16E72C4BAD26311 (tag_id), INDEX IDX_E16E72C4A94ADB61 (consumable_id), PRIMARY KEY(tag_id, consumable_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(191) NOT NULL, password VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, phone VARCHAR(255) DEFAULT NULL, git VARCHAR(255) DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', is_admin TINYINT(1) NOT NULL, enabled TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE consumable ADD CONSTRAINT FK_4475F0953DA5256D FOREIGN KEY (image_id) REFERENCES media_object (id)');
        $this->addSql('ALTER TABLE consumable ADD CONSTRAINT FK_4475F0954260F65B FOREIGN KEY (stock_place_id) REFERENCES stock_place (id)');
        $this->addSql('ALTER TABLE consumable_entrance ADD CONSTRAINT FK_FCEA8024A94ADB61 FOREIGN KEY (consumable_id) REFERENCES consumable (id)');
        $this->addSql('ALTER TABLE entity ADD CONSTRAINT FK_E284468517FE9FE FOREIGN KEY (equipment_id) REFERENCES equipment (id)');
        $this->addSql('ALTER TABLE equipment ADD CONSTRAINT FK_D338D5833DA5256D FOREIGN KEY (image_id) REFERENCES media_object (id)');
        $this->addSql('ALTER TABLE equipment ADD CONSTRAINT FK_D338D5834260F65B FOREIGN KEY (stock_place_id) REFERENCES stock_place (id)');
        $this->addSql('ALTER TABLE equipment_consumable ADD CONSTRAINT FK_6D7E044B517FE9FE FOREIGN KEY (equipment_id) REFERENCES equipment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE equipment_consumable ADD CONSTRAINT FK_6D7E044BA94ADB61 FOREIGN KEY (consumable_id) REFERENCES consumable (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE equipment_tag ADD CONSTRAINT FK_32097FE2517FE9FE FOREIGN KEY (equipment_id) REFERENCES equipment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE equipment_tag ADD CONSTRAINT FK_32097FE2BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lending ADD CONSTRAINT FK_74AB8C0381257D5D FOREIGN KEY (entity_id) REFERENCES entity (id)');
        $this->addSql('ALTER TABLE lending ADD CONSTRAINT FK_74AB8C03155210D6 FOREIGN KEY (user_lending_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE lending ADD CONSTRAINT FK_74AB8C0322949B31 FOREIGN KEY (user_return_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE lending ADD CONSTRAINT FK_74AB8C03A94ADB61 FOREIGN KEY (consumable_id) REFERENCES consumable (id)');
        $this->addSql('ALTER TABLE logging ADD CONSTRAINT FK_CA9562FAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE rfid_tag ADD CONSTRAINT FK_4FDECE21A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE rfid_tag ADD CONSTRAINT FK_4FDECE2181257D5D FOREIGN KEY (entity_id) REFERENCES entity (id)');
        $this->addSql('ALTER TABLE tag_consumable ADD CONSTRAINT FK_E16E72C4BAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tag_consumable ADD CONSTRAINT FK_E16E72C4A94ADB61 FOREIGN KEY (consumable_id) REFERENCES consumable (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE consumable_entrance DROP FOREIGN KEY FK_FCEA8024A94ADB61');
        $this->addSql('ALTER TABLE equipment_consumable DROP FOREIGN KEY FK_6D7E044BA94ADB61');
        $this->addSql('ALTER TABLE lending DROP FOREIGN KEY FK_74AB8C03A94ADB61');
        $this->addSql('ALTER TABLE tag_consumable DROP FOREIGN KEY FK_E16E72C4A94ADB61');
        $this->addSql('ALTER TABLE lending DROP FOREIGN KEY FK_74AB8C0381257D5D');
        $this->addSql('ALTER TABLE rfid_tag DROP FOREIGN KEY FK_4FDECE2181257D5D');
        $this->addSql('ALTER TABLE entity DROP FOREIGN KEY FK_E284468517FE9FE');
        $this->addSql('ALTER TABLE equipment_consumable DROP FOREIGN KEY FK_6D7E044B517FE9FE');
        $this->addSql('ALTER TABLE equipment_tag DROP FOREIGN KEY FK_32097FE2517FE9FE');
        $this->addSql('ALTER TABLE consumable DROP FOREIGN KEY FK_4475F0953DA5256D');
        $this->addSql('ALTER TABLE equipment DROP FOREIGN KEY FK_D338D5833DA5256D');
        $this->addSql('ALTER TABLE consumable DROP FOREIGN KEY FK_4475F0954260F65B');
        $this->addSql('ALTER TABLE equipment DROP FOREIGN KEY FK_D338D5834260F65B');
        $this->addSql('ALTER TABLE equipment_tag DROP FOREIGN KEY FK_32097FE2BAD26311');
        $this->addSql('ALTER TABLE tag_consumable DROP FOREIGN KEY FK_E16E72C4BAD26311');
        $this->addSql('ALTER TABLE lending DROP FOREIGN KEY FK_74AB8C03155210D6');
        $this->addSql('ALTER TABLE lending DROP FOREIGN KEY FK_74AB8C0322949B31');
        $this->addSql('ALTER TABLE logging DROP FOREIGN KEY FK_CA9562FAA76ED395');
        $this->addSql('ALTER TABLE rfid_tag DROP FOREIGN KEY FK_4FDECE21A76ED395');
        $this->addSql('DROP TABLE consumable');
        $this->addSql('DROP TABLE consumable_entrance');
        $this->addSql('DROP TABLE entity');
        $this->addSql('DROP TABLE equipment');
        $this->addSql('DROP TABLE equipment_consumable');
        $this->addSql('DROP TABLE equipment_tag');
        $this->addSql('DROP TABLE lending');
        $this->addSql('DROP TABLE logging');
        $this->addSql('DROP TABLE media_object');
        $this->addSql('DROP TABLE parameter');
        $this->addSql('DROP TABLE rfid_tag');
        $this->addSql('DROP TABLE stock_place');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE tag_consumable');
        $this->addSql('DROP TABLE user');
    }
}
