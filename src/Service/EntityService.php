<?php

namespace App\Service;

use App\Repository\EntityRepository;
use App\Repository\LendingRepository;

class EntityService
{
    /** @var LendingRepository */
    private $lendingRepository;

    /** @var EntityRepository */
    private $entityRepository;

    public function __construct(LendingRepository $lendingRepository, EntityRepository $entityRepository)
    {
        $this->lendingRepository = $lendingRepository;
        $this->entityRepository = $entityRepository;
    }

    public function getNoLendeds()
    {
        $lendings = $this->lendingRepository->findAllNoRetnurned();
        $allEntities = $this->entityRepository->findAll();

        $entities = [];

        foreach ($lendings as $lending) {
            $entity = $lending->getEntity();
            if ($entity !== null) {
                if (in_array($entity->getId(), $entities) === false) {
                    $entities[] = $entity->getId();
                }
            }
        }

        foreach ($allEntities as $key => $entity) {
            if (in_array($entity->getId(), $entities) === true) {
                unset($allEntities[$key]);
            }
        }
        return $allEntities;
    }

    public function isLended($entity_id) {
        return $this->lendingRepository->findOneBy([
            'entity' => $entity_id,
            'accepted' => 1,
            'returned' => 0
        ]);
    }
}