<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Repository\StockPlaceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *    normalizationContext={
 *    "groups"={"stockplace:read"},
 *    "skip_null_values" = false,
 * })
 * @ORM\Entity(repositoryClass=StockPlaceRepository::class)
 * @ApiFilter(SearchFilter::class, properties={"name": "partial"})
 */
class StockPlace
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"stockplace:read","equipment:read","consumable:read","stockplace-equipment:read","stockplace-consummable:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"stockplace:read","equipment:read","consumable:read","stockplace-equipment:read","stockplace-consummable:read"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Equipment::class, mappedBy="stockPlace")
     * @ORM\JoinColumn(nullable=true)
     * @ApiSubresource
     */
    private $equipments;

    /**
     * @ORM\OneToMany(targetEntity=Consumable::class, mappedBy="stockPlace")
     * @ORM\JoinColumn(nullable=true)
     * @ApiSubresource
     */
    private $consumables;

    public function __construct()
    {
        $this->equipments = new ArrayCollection();
        $this->consumables = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Equipment[]
     */
    public function getEquipments(): Collection
    {
        return $this->equipments;
    }

    public function addEquipment(Equipment $equipment): self
    {
        if (!$this->equipments->contains($equipment)) {
            $this->equipments[] = $equipment;
            $equipment->setStockPlace($this);
        }

        return $this;
    }

    public function removeEquipment(Equipment $equipment): self
    {
        if ($this->equipments->removeElement($equipment)) {
            // set the owning side to null (unless already changed)
            if ($equipment->getStockPlace() === $this) {
                $equipment->setStockPlace(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Consumable[]
     */
    public function getConsumables(): Collection
    {
        return $this->consumables;
    }

    public function addConsumable(Consumable $consumable): self
    {
        if (!$this->consumables->contains($consumable)) {
            $this->consumables[] = $consumable;
            $consumable->setStockPlace($this);
        }

        return $this;
    }

    public function removeConsumable(Consumable $consumable): self
    {
        if ($this->consumables->removeElement($consumable)) {
            // set the owning side to null (unless already changed)
            if ($consumable->getStockPlace() === $this) {
                $consumable->setStockPlace(null);
            }
        }

        return $this;
    }
}
