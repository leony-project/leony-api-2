<?php

namespace App\Controller\ConsumableEntrance;

use App\Entity\ConsumableEntrance;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class PostConsumableEntrance
{
    public function __invoke(ConsumableEntrance $data)
    {
        // Check and update stock
        if ($data->getEntrance() === true) {
            $data->getConsumable()->setQuantity(
                ($data->getConsumable()->getQuantity() === null ? 0 : $data->getConsumable()->getQuantity()) +
                $data->getQuantity()
            );
        } else {
            $newQuantity = ($data->getConsumable()->getQuantity() === null ? 0 : $data->getConsumable()->getQuantity()) - $data->getQuantity();

            if ($newQuantity < 0) {
                throw new BadRequestException('Insufficient stocks');
            }
                
            $data->getConsumable()->setQuantity($newQuantity);
        }

        return $data;
    }
}
