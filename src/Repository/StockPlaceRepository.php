<?php

namespace App\Repository;

use App\Entity\StockPlace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StockPlace|null find($id, $lockMode = null, $lockVersion = null)
 * @method StockPlace|null findOneBy(array $criteria, array $orderBy = null)
 * @method StockPlace[]    findAll()
 * @method StockPlace[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockPlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StockPlace::class);
    }

    // /**
    //  * @return StockPlace[] Returns an array of StockPlace objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StockPlace
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
