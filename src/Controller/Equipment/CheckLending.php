<?php

namespace App\Controller\Equipment;

use App\Service\ConsumableService;
use App\Service\EquipmentService;
use App\Service\LendingService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckLending
{
    private $equipmentService;
    private $lendingService;
    private $consumableService;

    public function __construct(
        EquipmentService $equipmentService,
        LendingService $lendingService,
        ConsumableService $consumableService
        )
    {
        $this->equipmentService = $equipmentService;
        $this->lendingService = $lendingService;
        $this->consumableService = $consumableService;

    }

    public function __invoke(Request $request){
        $data = json_decode(file_get_contents('php://input'));
        $error = [];
        $error['status'] = true;
        foreach ($data as $key => $d) {
            if (isset($tmp_error))
                unset($tmp_error);

            if (isset($tmp['entities']))
                unset($tmp['entities']);

            $id = $d->id;
            $lendingDatetime = $d->lendingDatetime; // ? A
            $returnDatetime = $d->returnDatetime; // ? B
            $quantity = $d->quantity;
            $type = $d->type;

            $tmp_error["status"] = false;
            $tmp_error["id"] = intval($id);
            $tmp_error['type'] = $type;
            $tmp_error['quantity'] = $quantity;

            if ($type == 'EQUIPMENT') {
                $equipment = $this->equipmentService->getEquipment($id);
                $entities = $equipment->getEntities();
                $tmp_error['entities'] = [];
                foreach ($entities as $e) {
                    $status = true;
                    $eid = $e->getId();
                    $lendingsEntity = $this->lendingService->checkLendings($eid);
                    if ($lendingsEntity != null) {
                        foreach ($lendingsEntity as $le) {
                            $existingLendingDatetime = $le->getLendingDatetime()->getTimeStamp()*1000; // ? C
                            $existingReturnDatetime = $le->getReturnDatetime()->getTimeStamp()*1000; // ? D
                            if ($returnDatetime >= $existingLendingDatetime && $returnDatetime <= $existingReturnDatetime) { // ? C - B - B - D  -> rend trop tard
                                if (!isset($tmp_error['returnMax']) || $tmp_error['returnMax'] > $existingLendingDatetime - (3600*1000)) {
                                    $tmp_error['returnMax'] = $existingLendingDatetime - (3600*1000);
                                }
                                $status = false;
                            } elseif ($lendingDatetime >= $existingLendingDatetime && $lendingDatetime <= $existingReturnDatetime) { // ? C - A - A - D -> demande trop tard
                                if (!isset($tmp_error['lendingMax']) || $tmp_error['lendingMax'] < $existingReturnDatetime + (3600*1000)) {
                                    $tmp_error['lendingMax'] = $existingReturnDatetime + (3600*1000);
                                }
                                $status = false;
                            } elseif ($lendingDatetime <= $existingLendingDatetime && $returnDatetime >= $existingReturnDatetime){ // ? A - C - D - B -> chevauche
                                $status = false;
                            } elseif ($lendingDatetime >= $existingLendingDatetime && $returnDatetime <= $existingReturnDatetime) { // ? C - A - B - D -> au milieu
                                $status = false;
                            }
                            if ($status) {
                                if (!in_array($eid, $tmp_error['entities']))
                                    $tmp_error['entities'][] = $eid;
                            } else {
                                if (array_search($eid, $tmp_error['entities']) !== false) 
                                    array_splice($tmp_error['entities'], array_search($eid, $tmp_error['entities']), 1);
                            }
                        }
                    } else {
                        $tmp_error['entities'][] = $eid;
                    }
                }
                if (isset($tmp_error['entities']) && !empty($tmp_error['entities'])) {
                    unset($tmp_error['returnMax']);
                    unset($tmp_error['lendingMax']);
                    $tmp_error["status"] = true;
                    if (count($tmp_error['entities']) < $quantity) {
                        $tmp_error['quantityMax'] = count($tmp_error['entities']);
                        $tmp_error["status"] = false;
                        $error['status'] = false;
                        unset($tmp_error['entities']);
                    }
                } else {
                    $error['status'] = false;
                }
            } elseif ($type == 'CONSUMABLE') {
                $consumable = $this->consumableService->getConsumable($id);
                $tmp_error['quantityMax'] = $consumable->getQuantity();
                if ($consumable->getQuantity() > $quantity) {
                    $tmp_error["status"] = true;
                } else {
                    $error['status'] = false;
                }
            }
            $error['data'][] = $tmp_error;
        }
        return new Response(json_encode($error));
    }
}