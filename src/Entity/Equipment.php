<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\EquipmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *  normalizationContext={
 *    "groups"={"equipment:read"},
 *    "skip_null_values" = false,
 *  },
 *  subresourceOperations={
 *     "api_consumables_equipments_get_subresource"={
 *      "method"="GET",
 *      "normalization_context"={"groups"={"equipment:read"}},
 *      "pagination_enabled"=true
 *    },
 *    "api_tags_equipments_get_subresource"={
 *     "method"="GET",
 *      "normalization_context"={"groups"={"equipment:read"}},
 *      "pagination_enabled"=true
 *    },
*     "api_stockplace_equipment_get_subresource"={
*         "method"="GET",
*         "normalization_context"={"groups"={"stockplace-equipment:read"}},
*         "pagination_enabled"=true
*     }
 * 
 *  },
 * 
 *  attributes={
 *      "order"={"id": "DESC"},  
 *  },
 * 
 *  itemOperations={
 *      "get",
 *      "put"={"security"="is_granted('ROLE_ADMIN')"},
 *      "patch"={"security"="is_granted('ROLE_ADMIN')"},
 *      "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *  },
 * 
 *  collectionOperations={
 *      "get",
 *      "post"={"security"="is_granted('ROLE_ADMIN')"}
 *  }
 * )
 * @ORM\Entity(repositoryClass=EquipmentRepository::class)
 * @ApiFilter(SearchFilter::class, properties={"name": "partial"})
 */

class Equipment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"entity:read","equipment:read","lending:read","user-lending:read","consumable:read","stockplace:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"entity:read","equipment:read","lending:read","user-lending:read","consumable:read","stockplace:read","stockplace-equipment:read"})
     * 
     * @Assert\NotBlank(
     *     message = "Name must be filled"
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"entity:read","equipment:read","consumable:read"})
     * 
     * @Assert\NotNull(
     *     message = "Enabled boolean must be filled"
     * )
     */
    private $enabled;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"entity:read","equipment:read","consumable:read"})
     */
    private $description;

    /**
     * @var MediaObject|null
     * 
     * @ORM\ManyToOne(targetEntity=MediaObject::class, cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"equipment:read","lending:read","consumable:read","stockplace:read","stockplace-equipment:read"})
     */
    public $image;

    /**
     * @ORM\OneToMany(targetEntity=Entity::class, mappedBy="equipment", orphanRemoval=true)
     * @ApiSubresource
     */
    private $entities;

    /**
     * @ORM\ManyToMany(targetEntity=Consumable::class, inversedBy="equipments")
     * @ApiSubresource
     */
    private $consumables;

    /**
     * @ORM\ManyToMany(targetEntity=Tag::class, inversedBy="equipments")
     * @Groups({"entity:read","equipment:read","consumable:read"})
     */
    private $tags;

    /**
     * @ORM\ManyToOne(targetEntity=StockPlace::class, inversedBy="equipments")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"entity:read","equipment:read","consumable:read"})
     */
    private $stockPlace;


    public function __construct()
    {
        $this->entities = new ArrayCollection();
        $this->consumables = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?MediaObject
    {
        return $this->image;
    }

    public function setImage(?MediaObject $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Entity[]
     */
    public function getEntities(): Collection
    {
        return $this->entities;
    }

    public function addEntity(Entity $entity): self
    {
        if (!$this->entities->contains($entity)) {
            $this->entities[] = $entity;
            $entity->setEquipment($this);
        }

        return $this;
    }

    public function removeEntity(Entity $entity): self
    {
        if ($this->entities->contains($entity)) {
            $this->entities->removeElement($entity);
            // set the owning side to null (unless already changed)
            if ($entity->getEquipment() === $this) {
                $entity->setEquipment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Consumable[]
     */
    public function getConsumables(): Collection
    {
        return $this->consumables;
    }

    public function addConsumable(Consumable $consumable): self
    {
        if (!$this->consumables->contains($consumable)) {
            $this->consumables[] = $consumable;
        }

        return $this;
    }

    public function removeConsumable(Consumable $consumable): self
    {
        if ($this->consumables->contains($consumable)) {
            $this->consumables->removeElement($consumable);
        }

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    /**
     * @Groups({"equipment:read","consumable:read"})
     */
    public function getEntitiesAvailable(): ?int
    {
        $entitiesAvailableNumber = 0;

        /** @var Entity $entity */
        foreach ($this->entities as $entity) {
            if ($entity->getAvailable() === true) {
                $entitiesAvailableNumber++;
            }
        }

        return $entitiesAvailableNumber;
    }

    public function getStockPlace(): ?StockPlace
    {
        return $this->stockPlace;
    }

    public function setStockPlace(?StockPlace $stockPlace): self
    {
        $this->stockPlace = $stockPlace;

        return $this;
    }
}
