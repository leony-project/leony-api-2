<?php

namespace App\Controller\Entity;

use App\Service\EntityService;

class GetNoLendeds
{
    /** @var EntityService */
    private $entityService;

    public function __construct(EntityService $entityService)
    {
        $this->entityService = $entityService;
    }

    public function __invoke()
    {
        return $this->entityService->getNoLendeds();
    }
}
