<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ConsumableEntranceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\ConsumableEntrance\PostConsumableEntrance;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *   normalizationContext={
 *    "groups"={"consumableEntrance:read"},
 *    "skip_null_values" = false,
 *  },
 *  subresourceOperations={
 *    "api_consumables_consumable_entrances_get_subresource"={
 *       "method"="GET",
 *       "normalization_context"={"groups"={"consumables-consumable_entrances:read"}},
 *       "pagination_enabled"=true
 *    }
 *  },
 * 
 *  attributes={
 *      "security"="is_granted('ROLE_ADMIN')",
 *      "order"={"date": "DESC"},
 *  },
 * 
 *  collectionOperations={
 *      "get",
 *      "post"={
 *          "controller"=PostConsumableEntrance::class
 *      }
 *  },
 * 
 *  itemOperations={
 *      "get",
 *      "patch",
 *      "delete"
 *  }
 * )
 * @ORM\Entity(repositoryClass=ConsumableEntranceRepository::class)
 */
class ConsumableEntrance
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"consumableEntrance:read","consumables-consumable_entrances:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"consumableEntrance:read","consumables-consumable_entrances:read"})
     * 
     * @Assert\NotNull(
     *      message = "Entrance must not be null."
     * )
     */
    private $entrance;

    /**
     * @ORM\ManyToOne(targetEntity=Consumable::class, inversedBy="consumableEntrances")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"consumableEntrance:read"})
     * 
     * @Assert\Valid
     */
    private $consumable;

    /**
     * @ORM\Column(type="float")
     * @Groups({"consumableEntrance:read","consumables-consumable_entrances:read"})
     * 
     * @Assert\NotNull(
     *      message = "Quantity must not be null."
     * )
     */
    private $quantity;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"consumableEntrance:read","consumables-consumable_entrances:read"})
     * 
     * @Assert\NotNull(
     *      message = "Date must not be null."
     * )
     */
    private $date;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntrance(): ?bool
    {
        return $this->entrance;
    }

    public function setEntrance(bool $entrance): self
    {
        $this->entrance = $entrance;

        return $this;
    }

    public function getConsumable(): ?Consumable
    {
        return $this->consumable;
    }

    public function setConsumable(?Consumable $consumable): self
    {
        $this->consumable = $consumable;

        return $this;
    }

    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    public function setQuantity(float $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
