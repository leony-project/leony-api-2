<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\LendingRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Controller\Lending\GetMyLendings;
use App\Controller\Lending\PostLending;
use App\Controller\Lending\PatchLending;

/**
 * @ApiResource(
 *  subresourceOperations={
 *    "api_users_lendings_get_subresource"={
 *      "method"="GET",
 *      "normalization_context"={
 *         "groups"={"user-lending:read"},
 *         "skip_null_values" = false,
 *      }
 *    },
 *    "api_entities_lendings_get_subresource"={
 *      "method"="GET",
 *      "normalization_context"={
 *         "groups"={"entity-lending:read"},
 *         "skip_null_values" = false,
 *      },
 *      "pagination_enabled"=true
 *    },
 *  },
 *
 *  normalizationContext={
 *    "groups"={"lending:read"},
 *    "skip_null_values" = false,
 *  },
 *
 *  attributes={
 *      "order"={"lendingDatetime": "DESC"},
 *      "security"="is_granted('ROLE_ADMIN')"
 *  },
 * 
 *  itemOperations={
 *      "get",
 *      "patch"={
 *          "controller"=PatchLending::class
 *      },
 *      "delete"
 *  },
 *  collectionOperations={
 *      "get_my_lendings"={
 *          "method"="GET",
 *          "security"="is_granted('ROLE_USER')",
 *          "path"="/users/me/lendings",
 *          "controller"=GetMyLendings::class,
 *          "normalization_context"={
 *              "groups"={"user-lending:read"},
 *              "skip_null_values" = false,
 *          }
 *      },
 *      "get",
 *      "post_lending"={
 *          "security"="is_granted('ROLE_USER')",
 *          "method"="POST",
 *          "path"="/lendings",
 *          "controller"=PostLending::class
 *      }
 *  }
 * 
 * )
 * @ORM\Entity(repositoryClass=LendingRepository::class)
 * @ApiFilter(BooleanFilter::class, properties={"returned","accepted"})
 *
 */
class Lending
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"entity-lending:read","lending:read","user-lending:read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Entity::class)
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"lending:read","user-lending:read","user-lending:read"})
     */
    private $entity;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"entity-lending:read","lending:read","user-lending:read"})
     */
    private $lendingDatetime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"entity-lending:read","lending:read","user-lending:read"})
     */
    private $returnDatetime;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"entity-lending:read","lending:read","user-lending:read"})
     * 
     * @Assert\NotNull(
     *     message = "Returned boolean must be filled"
     * )
     */
    private $returned;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $oldReturned;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"entity-lending:read","lending:read","user-lending:read"})
     *
     * @Assert\NotNull(
     *     message = "Accepted boolean must be filled"
     * )
     */
    private $accepted;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"entity-lending:read","lending:read","user-lending:read"})
     */
    private $commentLending;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"entity-lending:read","lending:read","user-lending:read"})
     */
    private $commentReturn;


    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="lendings")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"entity-lending:read","lending:read","user-lending:read"})
     *
     * @Assert\NotNull(
     *     message = "A lending must be linked to a user"
     * )
     *
     */
    private $userLending;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @Groups({"entity-lending:read","lending:read","user-lending:read"})
     */
    private $userReturn;

    /**
     * @ORM\ManyToOne(targetEntity=Consumable::class, inversedBy="lendings")
     * @Groups({"entity-lending:read","lending:read","project-lending:read","user-lending:read"})
     */
    private $consumable;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"entity-lending:read","lending:read","user-lending:read"})
     */
    private $lendingQuantity;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"entity-lending:read","lending:read","user-lending:read"})
     */
    private $returnQuantity;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Groups({"entity-lending:read","lending:read","user-lending:read"})
     */
    private $state;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntity(): ?Entity
    {
        return $this->entity;
    }

    public function setEntity(?Entity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getLendingDatetime(): ?\DateTimeInterface
    {
        return $this->lendingDatetime;
    }

    public function setLendingDatetime(?\DateTimeInterface $lendingDatetime): self
    {
        $this->lendingDatetime = $lendingDatetime;

        return $this;
    }

    public function getReturnDatetime(): ?\DateTimeInterface
    {
        return $this->returnDatetime;
    }

    public function setReturnDatetime(?\DateTimeInterface $returnDatetime): self
    {
        $this->returnDatetime = $returnDatetime;

        return $this;
    }

    public function getReturned(): ?bool
    {
        return $this->returned;
    }

    public function setReturned(bool $returned): self
    {
        $this->returned = $returned;

        return $this;
    }

    public function getOldReturned(): ?bool
    {
        return $this->oldReturned;
    }

    public function setOldReturned(bool $oldReturned): self
    {
        $this->oldReturned = $oldReturned;

        return $this;
    }

    public function getAccepted(): ?bool
    {
        return $this->accepted;
    }

    public function setAccepted(?bool $accepted): self
    {
        $this->accepted = $accepted;

        return $this;
    }


    public function getCommentLending(): ?string
    {
        return $this->commentLending;
    }

    public function setCommentLending(?string $commentLending): self
    {
        $this->commentLending = $commentLending;

        return $this;
    }

    public function getCommentReturn(): ?string
    {
        return $this->commentReturn;
    }

    public function setCommentReturn(?string $commentReturn): self
    {
        $this->commentReturn = $commentReturn;

        return $this;
    }

    public function getUserLending(): ?User
    {
        return $this->userLending;
    }

    public function setUserLending(?User $userLending): self
    {
        $this->userLending = $userLending;

        return $this;
    }

    public function getUserReturn(): ?User
    {
        return $this->userReturn;
    }

    public function setUserReturn(?User $userReturn): self
    {
        $this->userReturn = $userReturn;

        return $this;
    }


    public function getConsumable(): ?Consumable
    {
        return $this->consumable;
    }

    public function setConsumable(?Consumable $consumable): self
    {
        $this->consumable = $consumable;

        return $this;
    }

    public function getLendingQuantity(): ?int
    {
        return $this->lendingQuantity;
    }

    public function setLendingQuantity(?int $lendingQuantity): self
    {
        $this->lendingQuantity = $lendingQuantity;

        return $this;
    }

    public function getReturnQuantity(): ?int
    {
        return $this->returnQuantity;
    }

    public function setReturnQuantity(?int $returnQuantity): self
    {
        $this->returnQuantity = $returnQuantity;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }
      
}
