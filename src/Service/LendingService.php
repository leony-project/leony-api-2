<?php

namespace App\Service;

use App\Repository\LendingRepository;

class LendingService
{
    /** @var UserService */
    private $userService;

    public function __construct(
        UserService $userService,
        LendingRepository $lending_repo
    )
    {
        $this->userService = $userService;
        $this->lending_repo = $lending_repo;
    }

    public function getMyLendings()
    {
        $user = $this->userService->getLoggedUser();

        return $user->getLendings();
    }

    public function checkLendings($eid)
    {
        return $this->lending_repo->findLendingsOnEquipment($eid);
    }
}
