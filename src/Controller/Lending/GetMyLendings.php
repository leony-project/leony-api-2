<?php

namespace App\Controller\Lending;

use App\Service\LendingService;

class GetMyLendings
{
    /** @var LendingService */
    private $lendingService;

    public function __construct(LendingService $lendingService)
    {
        $this->lendingService = $lendingService;
    }

    public function __invoke()
    {
        return $this->lendingService->getMyLendings();
    }
}
