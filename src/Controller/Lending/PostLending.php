<?php

namespace App\Controller\Lending;

use App\Entity\Lending;
use App\Repository\LendingRepository;
use App\Service\LendingService;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class PostLending
{
    /** @var LendingService */
    private $lendingService;

    /** @var UserService */
    private $userService;

    /** @var LendingRepository */
    private $lendingRepository;

    public function __construct(
        LendingService $lendingService,
        UserService $userService,
        LendingRepository $lendingRepository
    )
    {
        $this->lendingService = $lendingService;
        $this->userService = $userService;
        $this->lendingRepository = $lendingRepository;
    }

    public function __invoke(Lending $data)
    {
        $user = $this->userService->getLoggedUser();

        $data->setOldReturned($data->getReturned());

        // Check and update stock
        if ($data->getReturned() === false) {
            if ($data->getEntity() !== null) {
                // Check if entity is already lended.
                // if ($this->lendingRepository->countInProgressByEntity($data->getEntity())[0]['lendings'] > 0) {
                //     throw new BadRequestException('This entity is already borrowed');
                // }

                // $data->getEntity()->setAvailable(false);
            }

            if ($data->getConsumable() !== null && $data->getLendingQuantity() !== null) {
                $newQuantity = ($data->getConsumable()->getQuantity() === null ? 0 : $data->getConsumable()->getQuantity()) - $data->getLendingQuantity();

                if ($newQuantity < 0) {
                    throw new BadRequestException('Insufficient stocks');
                }

                $data->getConsumable()->setQuantity($newQuantity);
            }
        } else {
            if ($data->getEntity() !== null) {
                $data->getEntity()->setAvailable(true);
            }
        }

        if ($user->hasRole('ROLE_ADMIN') === false) {
            // "Remove" some fields
            $data
                ->setReturned(false)
                ->setOldReturned(false)
                ->setAccepted(false)
                ->setCommentReturn(null)
                ->setUserReturn(null)
            ;
        }

        return $data;
    }
}
