<?php

namespace App\Repository;

use App\Entity\Entity;
use App\Entity\Lending;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Lending|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lending|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lending[]    findAll()
 * @method Lending[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LendingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lending::class);
    }

    public function countInProgressByEntity(Entity $entity)
    {
        return $this->createQueryBuilder('l')
            ->select('COUNT(l) AS lendings')
            ->where('l.entity = :entity AND l.returned = :false')
            ->setParameters([
                'entity' => $entity,
                'false' => false
            ])
            ->getQuery()
            ->getResult();
    }

    public function findAllNoRetnurned()
    {
        return $this->createQueryBuilder('l')
            ->innerJoin('l.entity', 'le')
            ->where('l.returned = :false')
            ->setParameters([
                'false' => false
            ])
            ->getQuery()
            ->getResult();
    }

    public function findLendingsOnEquipment($eid)
    {
        return $this->createQueryBuilder('l')
            ->innerJoin('l.entity', 'le')
            ->where('l.returned = false')
            ->andWhere('l.accepted = true')
            ->andWhere('l.entity = :id')
            ->setParameter('id', $eid)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Lending[] Returns an array of Lending objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lending
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
