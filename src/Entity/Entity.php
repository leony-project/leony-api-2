<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\Entity\GetNoLendeds;
use App\Controller\Entity\PostEntity;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;

/**
 * @ApiResource(
 *  subresourceOperations={
 *    "api_equipment_entities_get_subresource"={
 *      "method"="GET",
 *       "normalization_context"={"groups"={"equipment-entities:read"}},
 *      "pagination_enabled"=true
 *    }
 *  },
 *
 *  normalizationContext={
 *    "groups"={"entity:read"},
 *    "skip_null_values" = false
 *  },
 * 
 *  attributes={
 *      "order"={"id": "DESC"}
 *  },
 * 
 *  itemOperations={
 *      "get",
 *      "put"={"security"="is_granted('ROLE_ADMIN')"},
 *      "patch"={"security"="is_granted('ROLE_ADMIN')"},
 *      "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *  },
 * 
 *  collectionOperations={
 *      "get",
 *      "post_entity"={
 *          "security"="is_granted('ROLE_USER')",
 *          "method"="POST",
 *          "path"="/entities",
 *          "controller"=PostEntity::class
 *      },
 *      "get_no_lendeds"={
 *          "method"="GET",
 *          "path"="/entities/not-lendeds",
 *          "controller"=GetNoLendeds::class
 *      }
 *   }
 * )
 * @ORM\Entity(repositoryClass=EntityRepository::class)
 * @ApiFilter(BooleanFilter::class, properties={"enabled","available","enabled"})
 * @ApiFilter(SearchFilter::class, properties={"reference": "partial", "state": "exact"})
 */
class Entity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"entity:read","equipment-entities:read","lending:read","project-lending:read","user-lending:read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Equipment::class, inversedBy="entities")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"entity:read","lending:read","project-lending:read","user-lending:read"})
     * 
     * @Assert\NotNull(
     *     message = "An Entity must be linked to an equipment"
     * )
     */
    private $equipment;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"entity:read","equipment-entities:read","lending:read","project-lending:read","user-lending:read"})
     * 
     * @Assert\NotNull(
     *     message = "Enabled boolean must be filled"
     * )
     */
    private $enabled;

    /**
     * @ORM\OneToMany(targetEntity=Lending::class, mappedBy="entity")
     * @ApiSubresource
     */
    private $lendings;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"entity:read","equipment-entities:read","lending:read","project-lending:read","user-lending:read"})
     */
    private $reference;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"entity:read","equipment-entities:read","lending:read","project-lending:read","user-lending:read","consumable:read"})
     */
    private $available;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"entity:read","equipment-entities:read","lending:read","project-lending:read","user-lending:read","consumable:read"})
     */
    private $state;

    public function __construct()
    {
        $this->lendings = new ArrayCollection();
        $this->available = true;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEquipment(): ?Equipment
    {
        return $this->equipment;
    }

    public function setEquipment(?Equipment $equipment): self
    {
        $this->equipment = $equipment;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return Collection|Lending[]
     */
    public function getLendings(): Collection
    {
        return $this->lendings;
    }

    public function addLending(Lending $lending): self
    {
        if (!$this->lendings->contains($lending)) {
            $this->lendings[] = $lending;
            $lending->setEntity($this);
        }

        return $this;
    }

    public function removeLending(Lending $lending): self
    {
        if ($this->lendings->contains($lending)) {
            $this->lendings->removeElement($lending);
            // set the owning side to null (unless already changed)
            if ($lending->getEntity() === $this) {
                $lending->setEntity(null);
            }
        }

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getAvailable(): ?bool
    {
        return $this->available;
    }

    public function setAvailable(bool $available): self
    {
        $this->available = $available;

        return $this;
    }

    public function getState(): ?String
    {
        return $this->state;
    }

    public function setState(?String $state): self
    {
        $this->state = $state;

        return $this;
    }
}
