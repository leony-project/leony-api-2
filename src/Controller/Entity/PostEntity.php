<?php

namespace App\Controller\Entity;

use App\Entity\Entity;
use App\Repository\EntityRepository;
use LogicException;

class PostEntity
{
    /** @var EntityRepository */
    private $entityRepository;

    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    public function __invoke(Entity $data)
    {
        $entitiesWithSameRef = $this->entityRepository->findBy([
            'reference' => $data->getReference()
        ]);

        if (count($entitiesWithSameRef) > 0) {
            throw new LogicException('An existing entity with this reference already exist.');
        }

        return $data;
    }
}