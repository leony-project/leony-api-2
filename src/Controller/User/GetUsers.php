<?php

namespace App\Controller\User;

use App\Service\UserService;

class GetUsers
{
    /** @var UserService */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function __invoke($data)
    {
        $user = $this->userService->getLoggedUser();

        return $data;
    }
}
