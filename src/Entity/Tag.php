<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Repository\TagRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *      attributes={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      },
 *     attributes={"order"={"id": "DESC"}},
 *     normalizationContext={
 *         "groups"={"tag:read"},
 *         "skip_null_values" = false,
 *     },
 * )
 * @ORM\Entity(repositoryClass=TagRepository::class)
 */
class Tag
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"consumable:read","equipment-consumable:read","equipment:read","entity:read","tag:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"consumable:read","equipment-consumable:read","equipment:read","entity:read","tag:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=30)
     * @Groups({"consumable:read","equipment-consumable:read","equipment:read","tag:read"})
     */
    private $color;

    /**
     * @ORM\ManyToMany(targetEntity=Consumable::class, inversedBy="tags")
     * @ApiSubresource
     */
    private $consumable;

    /**
     * @ORM\ManyToMany(targetEntity=Equipment::class, mappedBy="tags")
     * @ApiSubresource
     */
    private $equipments;

    public function __construct()
    {
        $this->consumable = new ArrayCollection();
        $this->equipments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return Collection|Consumable[]
     */
    public function getConsumable(): Collection
    {
        return $this->consumable;
    }

    public function addConsumable(Consumable $consumable): self
    {
        if (!$this->consumable->contains($consumable)) {
            $this->consumable[] = $consumable;
        }

        return $this;
    }

    public function removeConsumable(Consumable $consumable): self
    {
        if ($this->consumable->contains($consumable)) {
            $this->consumable->removeElement($consumable);
        }

        return $this;
    }

    /**
     * @return Collection|Equipment[]
     */
    public function getEquipments(): Collection
    {
        return $this->equipments;
    }

    public function addEquipment(Equipment $equipment): self
    {
        if (!$this->equipments->contains($equipment)) {
            $this->equipments[] = $equipment;
            $equipment->addTag($this);
        }

        return $this;
    }

    public function removeEquipment(Equipment $equipment): self
    {
        if ($this->equipments->contains($equipment)) {
            $this->equipments->removeElement($equipment);
            $equipment->removeTag($this);
        }

        return $this;
    }

}
