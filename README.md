# Leony Manager - API

Leony Manager is a manager opensource for all fablab.

## Requirements 

- Install [PHP 7.4](https://www.php.net/downloads.php)
- Install [composer](https://getcomposer.org/download/)
- Install [symfony cli](https://symfony.com/download)

## Installation

- Clone the repository
```
git clone git@gitlab.com:leony-project/leony-api-2.git
```

- Copy `.env` to `.env.local` and configure your database connection 
- Install dependencies with composer
```
composer install
```

- Create a private and public key couple to sign JWT. Store them in `config/jwt` 
```
openssl genrsa -out private.pem 2048
openssl rsa -in private.key -pubout -out public.pem
```
- Install your database
```
php bin/console doctrine:schema:create
```

- Launch the API
```
symfony server:start
```
