<?php

namespace App\Service;

use App\Entity\RfidTag;
use App\Repository\RfidTagRepository;
use Doctrine\ORM\EntityManagerInterface;

class RfidTagService
{
    private $rfidRepository;
    private $em;

    public function __construct(RfidTagRepository $rfidRepository, EntityManagerInterface $em)
    {
        $this->rfidRepository = $rfidRepository;
        $this->em = $em;
    }

    public function getOneById($id) {
        return $this->rfidRepository->findOneBy(['uid' => $id]);
    }

    public function register($id) {
        $tag = new RfidTag();
        $tag->setUid($id);
        $this->em->persist($tag);
        $this->em->flush();
        return $tag;
    }
}
